const { GraphQLServer } = require('graphql-yoga')
const { prisma } = require('./generated/prisma-client')
const info = require('./resolvers/info')
const feed = require('./resolvers/feed')
const post = require('./resolvers/post')

const typeDefs = './schema.graphql'

const resolvers = {
  Query: {
    info,
    feed
  },
  Mutation: {
    post
  },
}

const server = new GraphQLServer({
  typeDefs,
  resolvers,
  context: { prisma },
  // context: request => ({
  //   ...request,
  //   prisma,
  // }),
})

server.start(() => {
  console.log('Server is running on http://localhost:4000')
})