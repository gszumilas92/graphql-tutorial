function post (root, args, context) {
  return context.prisma.createLink({
    url: args.url,
    description: args.description,
  })
}

module.exports = post;
